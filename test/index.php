<?php
header('Content-Type:application/json');
error_reporting(E_ALL);
ini_set('display_errors', true);
use \PiecesPHP\Core\Database\ActiveRecordModel;

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../src/Core/Database/ActiveRecordModel.php';

try {
    $model = new ActiveRecordModel(['driver' => 'sqlite', 'database' => './sqlite.sq3', 'table' => 'test']);
    
    $pdo = $model->getDb();

    $pdo->exec('CREATE TABLE IF NOT EXISTS test (id INTEGER PRIMARY KEY, name VARCHAR (255) NOT NULL)');
    $pdo->exec('CREATE TABLE IF NOT EXISTS test2 (id INTEGER PRIMARY KEY, name VARCHAR (255) NOT NULL)');

    $data = [
        'SELECT' => [
            'success' => false,
            'resultSet' => [],
            'query' => '',
        ],
        'INSERT' => [
            'success' => false,
            'insertId' => 0,
            'lastInsertItem' => null,
            'query' => '',
        ],
        'UPDATE' => [
            'success' => false,
            'updateItems' => null,
            'query' => '',
            'querySelect' => '',
        ],
    ];

    $data['SELECT']['success'] = $model->select()->execute();
    $data['SELECT']['resultSet'] = $model->result();
    $data['SELECT']['query'] = $model->getLastSQLExecuted();

    $data['INSERT']['success'] = $model->insert([
        'name' => 'TEST: ' . rand(0, 3000)
    ])->execute();
    $data['INSERT']['insertId'] = $model->lastInsertId();
    $data['INSERT']['lastInsertItem'] = $model->get($model->lastInsertId(), 'id', ['name AS test_alias']);
    $data['INSERT']['query'] = $model->getLastSQLExecuted();

    $where_update = [
        'id' => [
            '=' => 3,
        ],
    ];
    $data['UPDATE']['success'] = $model->update([
        'name' => 'UPDATE TEST: ' . rand(0, 3000)
    ])->where($where_update)->execute();
    $data['UPDATE']['query'] = $model->getLastSQLExecuted();
    $model->select()->where($where_update)->execute();
    $data['UPDATE']['updateItems'] = $model->result();
    $data['UPDATE']['querySelect'] = $model->getLastSQLExecuted();

   
    echo json_encode($data, \JSON_PRETTY_PRINT);

} catch (\Exception $e) {
    var_dump($e);
}