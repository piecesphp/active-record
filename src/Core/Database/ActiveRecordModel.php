<?php

/**
 * ActiveRecordModel.php
 */
namespace PiecesPHP\Core\Database;

use \PDO;
use \PASVL\Traverser\VO\Traverser;
use \PASVL\ValidatorLocator\ValidatorLocator;
use PiecesPHP\Core\Database\Exceptions\RequiredOptionMissedException;
use PiecesPHP\Core\Database\Exceptions\OptionPatternMismatchException;

/**
 * ActiveRecordModel - Implementación básica de modelo ActiveRecord.
 *
 * @package     PiecesPHP\Core\Database
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @version     v2.0.0
 * @copyright   Copyright (c) 2018
 */
class ActiveRecordModel implements \JsonSerializable
{

	/** @var \PDO $db Instancia de \PDO */
	protected $db = null;

	/** @var string Nombre de la tabla */
	protected $table = null;

	/**
	 * $prefix_table
	 * 
	 * Prefijo de la tabla
	 *
	 * @var string
	 */
	protected $prefix_table = null;

	/** @var array $fields Array que representa las columnas con la estructura ['columna1','columna2'[,...]] */
	private $fields = null;

	/**
	 * $type_result
	 *
	 * Define si el resultados de las consultas de selección de tipo objeto o array asociativo
	 * 
	 * @var bool
	 */
	protected $type_result_assoc = false;

	/** @var \PDOStatement $prepareStatement Consulta preparada*/
	protected $prepareStatement = null;

	/** @ignore @var $sql */
	protected $sql = "";

	/** @ignore @var $lastSQLExecuted */
	protected $lastSQLExecuted = "";

	/** @ignore @var $selectFields */
	protected $selectFields = "*";

	/** @ignore @var $selectString */
	protected $selectString = "";

	/** @ignore @var $insertString */
	protected $insertString = "";

	/** @ignore @var $updateString */
	protected $updateString = "";

	/** @ignore @var $deleteString */
	protected $deleteString = "";

	/** @ignore @var $joinsString */
	protected $joinsString = [];

	/** @ignore @var $whereString */
	protected $whereString = "";

	/** @ignore @var $orderByString */
	protected $orderByString = "";

	/** @ignore @var $action */
	protected $action = null;

	/** @ignore @var $groupByString */
	protected $groupByString = "";

	/** @ignore @var array|null $replacePrepareValues Array con los alias de la consulta preparada y sus valores con la estructura [':alias'=>valor[,...]] */
	protected $replacePrepareValues = [];

	/** @ignore @var array $whereReplacePrepareValues Array con los alias de la consulta preparada y sus valores con la estructura usado para los alias en where[':alias'=>valor[,...]] */
	protected $whereReplacePrepareValues = [];

	/** @ignore @var array $resultSet */
	protected $resultSet = null;

	/**
	 * $selectClass
	 *
	 * @var string Clase que intentará simular en los selects cuando se haga con PDO::FETCH_CLASS
	 */
	protected $selectClass = null;

	/**
	 * $defaultValueOptions
	 * 
	 * @var array Opciones por defecto
	 */
	protected static $defaultValueOptions = [
		'driver' => 'mysql',
		'database' => '',
		'host' => 'localhost',
		'user' => 'root',
		'password' => '',
		'charset' => 'utf8',
		'config' => true,
		'table' => 'NOT_DEFINED_TABLE',
	];

	/**
	 * $patternsOptions
	 * 
	 * @var array Patrones para validar las opciones
	 */
	protected static $patternsOptions = [
		'driver' => [
			'driver' => ':string'
		],
		'database' => [
			'database' => ':string'
		],
		'host' => [
			'host' => ':string'
		],
		'user' => [
			'user' => ':string'
		],
		'password' => [
			'password' => ':string'
		],
		'charset' => [
			'charset' => ':string'
		],
		'config' => [
			'config' => ':bool'
		],
		'table' => [
			'table' => ':string'
		],
	];

	/**
	 * $optionsDb
	 *
	 * @var array Opciones de configuración de la base de datos
	 */
	protected static $optionsDb = [];

    //Acciones
	const INSERT = 'INSERT';
	const SELECT = 'SELECT';
	const UPDATE = 'UPDATE';
	const DELETE = 'DELETE';

	//Comentarios
	const DEFAULT_OPTIONS_COMMENTS = [
		'driver' => '(string) El controlador PDO.  Opcional. Por defecto mysql',
		'database' => '(string) Nombre de la base de datos. Obligatorio',
		'host' => '(string) Servidor. Opcional. Por defecto localhost',
		'user' => '(string) Usuario. Opcional. Por defecto root',
		'password' => '(string) Contraseña. Opcional. Por defecto una cadena vacía',
		'charset' => '(string) El juego de caracteres. Opcional. Por defecto utf8',
		'config' => '(bool) Si se configurará la instancia PDO. Opcional. Por defecto true',
		'table' => '(string) El nombre de la tabla. Opcional. Por defecto NOT_DEFINED_TABLE',
	];

	/**
	 * Configuración del modelo.
	 *
	 * Usa PDO para la conexión.
	 *
	 * @param array $options Las opciones de configuración (ver la constante de clase DEFAULT_OPTIONS_COMMENTS)
	 * @return static
	 * @throws RequiredOptionMissedException|OptionPatternMismatchException
	 */
	public function __construct(array $options = [])
	{
		if (!array_key_exists('config', $options)) {
			$options['config'] = static::$defaultValueOptions['config'];
		} elseif (!is_bool($options['config'])) {
			$options['config'] = null;
		}

		$config = $options['config'];

		if ($config) {
			$this->configDb($options);
		} else {

			if (is_string($this->prefix_table)) {
				$this->table = trim($this->prefix_table) . trim($this->table);
			}

			if (is_null($this->fields) && is_string($this->table)) {
				$this->configFields();
			}
		}
	}

	/**
	 * Prepara una consulta.
	 * @param string $sql Sentencia SQL
	 * @return \PDOStatement
	 * Devuelve la consulta preparada.
	 */
	public function prepare(string $sql)
	{
		try {
			$this->prepareStatement = $this->db->prepare($sql);
			return $this->prepareStatement;
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	/**
	 * Prepara la consulta de selección la base de datos.
	 * @param array|string $fields
	 * Puede ser un array con la estructura ['columna','columna2'[,...]].
	 * O un string tal como 'columna1,columna2[,...]'
	 *
	 * @return static
	 * Devuelve el objeto mismo.
	 */
	public function select($fields = null)
	{
		if (!is_null($fields)) {
			if (is_array($fields)) {
				if (count($fields) > 0) {
					$fields_select = [];

					foreach ($fields as $field) {
						$fields_select[] = $field;
					}
					$fields_select = implode(',', $fields_select);
					$this->selectFields = $fields_select;
				} else {
					$this->selectFields = "*";
				}
			} else if (is_string($fields)) {
				$this->selectFields = $fields;
			} else {
				throw new \Exception("Unexpected param type");
			}
		} else {
			$this->selectFields = "*";
		}
		$this->selectString = "SELECT $this->selectFields FROM $this->table ";
		$this->action = self::SELECT;
		return $this;
	}

	/**
	 * Prepara la consulta de inserción de un registro en la base de datos.
	 * @param array $data un array asociativo con la estructura ['columna'=>valor[,...]]
	 * @return static
	 * Devuelve el objeto mismo.
	 */
	public function insert(array $data)
	{
		try {
			$fields = [];
			$values = [];
			$values_alias = [];

			foreach ($data as $field => $value) {
				$fields[] = $field;
				$values_alias[] = ":$field";
				$values[":$field"] = $value;
			}

			$fields = implode(',', $fields);
			$values_alias = implode(',', $values_alias);
			$this->replacePrepareValues = $values;

			$this->insertString = "INSERT INTO $this->table ($fields) VALUES ($values_alias) ";

			$this->action = self::INSERT;
			return $this;
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	/**
	 * Prepara la consulta de actualización de registros en la base de datos.
	 * Debería ser acompañado con el método where.
	 * @param array $update_pair un array asociativo con la estructura ['columna'=>valor[,...]]
	 * @return static
	 * Devuelve el objeto mismo.
	 */
	public function update(array $update_pair)
	{
		try {
			$values = [];
			$fields_values = [];

			foreach ($update_pair as $field => $value) {
				$alias = ":$field";
				$values[$alias] = $value;
				$fields_values[] = $field . "=" . $alias;
			}

			$this->replacePrepareValues = $values;

			$this->updateString = implode(',', $fields_values);
			$this->updateString = "UPDATE $this->table SET $this->updateString ";

			$this->action = self::UPDATE;

			return $this;
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	/**
	 * Prepara la consulta de la eliminación de registros en la base de datos.
	 * @param mixed $where_string Argumento tal como se pasaría a la función where
	 * @return static
	 * Devuelve el objeto mismo.
	 */
	public function delete($where)
	{
		try {
			$this->action = self::DELETE;
			$this->deleteString = " DELETE FROM $this->table ";
			return $this->where($where);
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	/**
	 * Establece un join de una consulta
	 * 
	 * @param string $table
	 * @param string|array|bool $on un string de los criterios de WHERE o un array como:
	 * [
	 * 		'columna' => ['operator' => 'value', 'and_or' => 'AND|OR'], //Si and_or se omite, por defecto es AND
	 * 		...
	 * ]
	 * o
	 * [
	 * 		'columna' => 'value', //Donde se asume que el operador es = y si hay varios se asume AND
	 * 		...
	 * ]
	 * @param string $type LEFT|INNER|RIGHT|NORMAL
	 * @return static
	 * @throws \Exception
	 * Devuelve el objeto mismo.
	 */
	public function join(string $table, $on = false, string $type = 'NORMAL')
	{
		$table_join = null;
		$on_string = null;

		if (is_array($on)) {//Verifica si el $on es un array
			$on_string = [];
			$num_criteries = count($on);//Cantidad de criterios
			$current_critery = 0;//Criterio actual
			foreach ($on as $column => $value) {

				$current_critery++;

				//Verifica que $column sea string puesto que debe corresponder al nombre de columna
				if (is_string($column)) {

					$alias = ":" . $column . (time() + rand(0, 500));
					$alias = str_replace(['{THIS}', '{DEFAULT}', '.'], [$table, $this->table, ''], $alias);

					if (is_array($value)) {//Verifica si $value es un array para buscar la clave 'operator' y 'and_or'

						if (count($value) > 0 && count($value) < 3) {//Verifica que $value contenga 2 elementos como máximo

							$and_or = isset($value['and_or']) ? $value['and_or'] : 'AND';
							$and_or = $and_or == 'AND' || $and_or == 'OR' ? $and_or : 'AND';

							foreach ($value as $key => $value_need) {

								if ($key == 'and_or') {//Ignora la clave 'and_or'
									continue;
								}

								//Verifica que $key sea un string y que $value_need sea string|numérico
								if (is_string($key) && (is_string($value_need) || is_numeric($value_need))) {

									$value_need = str_replace(['{THIS}', '{DEFAULT}'], [$table, $this->table], $value_need);

									if ($num_criteries > 1) {

										if ($current_critery < $num_criteries) {

											$on_string[] = "$column $key $value_need $and_or";

										} else {

											$on_string[] = "$column $key $value_need";

										}
									} else {

										$on_string[] = "$column $key $value_need";

									}
								} else {
									throw $this->throwException('malformed_param');
								}

							}

						} else {
							throw $this->throwException('malformed_param');
						}

					} else if (is_string($value) || is_numeric($value)) {//Verifica que $value sea un string

						$value = str_replace(['{THIS}', '{DEFAULT}'], [$table, $this->table], $value);

						if ($num_criteries > 1) {

							if ($current_critery < $num_criteries) {

								$on_string[] = "$column = $value AND";

							} else {

								$on_string[] = "$column = $value";

							}

						} else {

							$on_string[] = "$column = $value";

						}

					} else {
						throw $this->throwException('malformed_param');
					}

				} else {
					throw $this->throwException('malformed_param');
				}

			}

			$on_string = implode(' ', $on_string); //Convertir $on_string en string para la consulta

			$on_string = "ON $on_string";

		} else if (is_string($on)) {//Verifica que $on sea un string

			$on_string = "ON $on";

		} else if ($on === false) {//Verifica que $on sea un string

			$on_string = '';

		} else {

			throw $this->throwException('malformed_param');

		}

		$on_string = str_replace(['{THIS}', '{DEFAULT}'], [$table, $this->table], $on_string);

		switch ($type) {
			case 'LEFT':
				$type = 'LEFT';
				break;
			case 'RIGHT':
				$type = 'RIGHT';
				break;
			case 'INNER':
				$type = 'INNER';
				break;
			case 'NORMAL':
			default:
				$type = '';
				break;
		}
		$this->joinsString[$table] = " $type JOIN $table $on_string";

		return $this;
	}

	/**
	 * Establece un LEFT join de una consulta
	 * 
	 * @param string $table
	 * @param string|array|bool $on un string de los criterios de WHERE o un array como:
	 * [
	 * 		'columna' => ['operator' => 'value', 'and_or' => 'AND|OR'], //Si and_or se omite, por defecto es AND
	 * 		...
	 * ]
	 * o
	 * [
	 * 		'columna' => 'value', //Donde se asume que el operador es = y si hay varios se asume AND
	 * 		...
	 * ]
	 * @return static
	 * @throws \Exception
	 * Devuelve el objeto mismo.
	 */
	public function leftJoin(string $table, $on = false)
	{
		return $this->join($table, $on, 'LEFT');
	}

	/**
	 * Establece un RIGHT join de una consulta
	 * 
	 * @param string $table
	 * @param string|array|bool $on un string de los criterios de WHERE o un array como:
	 * [
	 * 		'columna' => ['operator' => 'value', 'and_or' => 'AND|OR'], //Si and_or se omite, por defecto es AND
	 * 		...
	 * ]
	 * o
	 * [
	 * 		'columna' => 'value', //Donde se asume que el operador es = y si hay varios se asume AND
	 * 		...
	 * ]
	 * @return static
	 * @throws \Exception
	 * Devuelve el objeto mismo.
	 */
	public function rightJoin(string $table, $on = false)
	{
		return $this->join($table, $on, 'RIGHT');
	}

	/**
	 * Establece un INNER join de una consulta
	 * 
	 * @param string $table
	 * @param string|array|bool $on un string de los criterios de WHERE o un array como:
	 * [
	 * 		'columna' => ['operator' => 'value', 'and_or' => 'AND|OR'], //Si and_or se omite, por defecto es AND
	 * 		...
	 * ]
	 * o
	 * [
	 * 		'columna' => 'value', //Donde se asume que el operador es = y si hay varios se asume AND
	 * 		...
	 * ]
	 * @return static
	 * @throws \Exception
	 * Devuelve el objeto mismo.
	 */
	public function innerJoin(string $table, $on = false)
	{
		return $this->join($table, $on, 'INNER');
	}

	/**
	 * Establece los criterios de WHERE para la consulta.
	 * @param string|array $where un string de los criterios de WHERE o un array como:
	 * [
	 * 		'columna' => ['operator' => 'value', 'and_or' => 'AND|OR'], //Si and_or se omite, por defecto es AND
	 * 		...
	 * ]
	 * o
	 * [
	 * 		'columna' => 'value', //Donde se asume que el operador es = y si hay varios se asume AND
	 * 		...
	 * ]
	 * @return static
	 * @throws \Exception
	 * Devuelve el objeto mismo.
	 */
	public function where($where)
	{
		$where_string = null;

		$this->whereReplacePrepareValues = [];

		if (is_array($where)) {//Verifica si el $where es un array
			$where_string = [];
			$num_criteries = count($where);//Cantidad de criterios
			$current_critery = 0;//Criterio actual
			foreach ($where as $column => $value) {

				$current_critery++;

				//Verifica que $column sea string puesto que debe corresponder al nombre de columna
				if (is_string($column)) {

					if (is_array($value)) {//Verifica si $value es un array para buscar la clave 'operator' y 'and_or'

						if (count($value) > 0 && count($value) < 3) {//Verifica que $value contenga 2 elementos como máximo

							$and_or = isset($value['and_or']) ? $value['and_or'] : 'AND';
							$and_or = $and_or == 'AND' || $and_or == 'OR' ? $and_or : 'AND';

							foreach ($value as $key => $value_need) {
								if ($key == 'and_or') {//Ignora la clave 'and_or'
									continue;
								}
								//Verifica que $key sea un string y que $value_need sea string|numérico
								if (is_string($key) && (is_string($value_need) || is_numeric($value_need))) {
									if ($num_criteries > 1) {
										if ($current_critery < $num_criteries) {

											$alias = ":" . $column . (time() + rand(0, 500));
											$alias = str_replace(['.'], [''], $alias);

											$where_string[] = "$column $key $alias $and_or";

											$this->whereReplacePrepareValues[$alias] = $value_need;

										} else {

											$alias = ":" . $column . (time() + rand(0, 500));
											$alias = str_replace(['.'], [''], $alias);

											$where_string[] = "$column $key $alias";

											$this->whereReplacePrepareValues[$alias] = $value_need;

										}
									} else {

										$alias = ":" . $column . (time() + rand(0, 500));
										$alias = str_replace(['.'], [''], $alias);

										$where_string[] = "$column $key $alias";

										$this->whereReplacePrepareValues[$alias] = $value_need;
									}
								} else {
									throw $this->throwException('malformed_param');
								}

							}

						} else {
							throw $this->throwException('malformed_param');
						}

					} else if (is_string($value) || is_numeric($value)) {//Verifica que $value sea un string

						if ($num_criteries > 1) {

							if ($current_critery < $num_criteries) {

								$alias = ":" . $column . (time() + rand(0, 500));
								$alias = str_replace(['.'], [''], $alias);

								$where_string[] = "$column = $alias AND";

								$this->whereReplacePrepareValues[$alias] = $value;

							} else {

								$alias = ":" . $column . (time() + rand(0, 500));
								$alias = str_replace(['.'], [''], $alias);

								$where_string[] = "$column = $alias";

								$this->whereReplacePrepareValues[$alias] = $value;

							}

						} else {

							$alias = ":" . $column . (time() + rand(0, 500));
							$alias = str_replace(['.'], [''], $alias);

							$where_string[] = "$column = $alias";

							$this->whereReplacePrepareValues[$alias] = $value;
						}

					} else {
						throw $this->throwException('malformed_param');
					}

				} else {
					throw $this->throwException('malformed_param');
				}

			}

			$where_string = implode(' ', $where_string); //Convertir $where_string en string para la consulta

		} else if (is_string($where)) {//Verifica que $where sea un string

			$where_string = $where;

		} else {

			throw $this->throwException('malformed_param');

		}
		$this->whereString = " WHERE $where_string ";
		return $this;
	}

	/**
	 * Obtiene todos los registros de la tabla.
	 * @param bool $assoc Define si el resultado es un array asociativo o un objeto
	 * @return boolean|array
	 * Devuelve un array de objetos o false en caso de error.
	 */
	public function getAll(bool $assoc = false)
	{
		try {

			$prepareStatement = $this->prepare("SELECT * FROM $this->table");

			$status = $prepareStatement->execute();

			if ($status) {

				if ($this->type_result_assoc === true || $assoc === true) {
					$fetch = $this->prepareStatement->fetchAll(PDO::FETCH_ASSOC);
				} else {
					$fetch = $this->prepareStatement->fetchAll(PDO::FETCH_OBJ);
				}

				$this->closeCursor();
				return $fetch;
			} else {
				return false;
			}
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	/**
	 * Ejecuta la consulta de tipo SELECT que esté preparada.
	 * @param bool $assoc Define si el resultado es un array asociativo o un objeto
	 * @return boolean|object|array|int
	 * Devuelve un objeto correspondiente al primer registro que devuelva la consulta.
	 * Si la consulta preparada no es de tipo SELECT devuelve false.
	 * Si la consulta no da resultados devuelve -1.
	 * Si la consulta falla devuelve false.
	 */
	public function row(bool $assoc = false)
	{
		switch ($this->action) {
			case self::SELECT:
				$this->structureQuery('SELECT');
				if (count($this->whereReplacePrepareValues) > 0) {
					$status = $this->prepareStatement->execute($this->whereReplacePrepareValues);
				} else {
					$status = $this->prepareStatement->execute();
				}
				if ($status) {
					if ($this->type_result_assoc === true || $assoc === true) {
						$fetch = $this->prepareStatement->fetchAll(PDO::FETCH_ASSOC);
					} else {
						$fetch = $this->prepareStatement->fetchAll(PDO::FETCH_OBJ);
					}

					if (count($fetch) > 0) {
						$fetch = $fetch[0];
					} else {
						$fetch = -1;
					}

					$this->closeCursor();
					return $fetch;
				} else {
					return false;
				}
				break;
			default:
				return false;
				break;
		}
	}

	/**
	 * Trae un registro según el criterio brindado
	 * 
	 * @param mixed $value El valor de comparación
	 * @param string $column_name La columna de comparación
	 * @param string|array $select_columns Las columnas que devolverá la consulta
	 * @param bool $assoc Define si el resultado es un array asociativo o un objeto
	 * 
	 * @return boolean|object|array|int
	 * 
	 * Devuelve un objeto correspondiente al primer registro que devuelva la consulta.
	 * Si la consulta no da resultados devuelve -1.
	 * Si la consulta falla devuelve false.
	 */
	public function get($value, string $column_name = 'id', $select_columns = '*', bool $assoc = false)
	{
		$fetch = null;

		if (!is_null($select_columns)) {

			if (is_array($select_columns)) {

				if (count($select_columns) > 0) {

					$fields_select = [];

					foreach ($select_columns as $field) {
						$fields_select[] = $field;
					}

					$fields_select = implode(',', $fields_select);

					$select_columns = $fields_select;

				} else {
					$select_columns = "*";
				}

			} else if (is_string($select_columns)) {

				$select_columns = $select_columns;

			} else {
				throw new \Exception("Unexpected param type");
			}
		} else {
			$select_columns = "*";
		}


		$this->prepareStatement = $this->prepare("SELECT $select_columns FROM $this->table WHERE $column_name = :VALUE_WHERE");



		$status = $this->prepareStatement->execute([
			':VALUE_WHERE' => $value,
		]);

		if ($status) {
			if ($this->type_result_assoc === true || $assoc === true) {
				$fetch = $this->prepareStatement->fetchAll(PDO::FETCH_ASSOC);
			} else {
				$fetch = $this->prepareStatement->fetchAll(PDO::FETCH_OBJ);
			}

			if (count($fetch) > 0) {
				$fetch = $fetch[0];
			} else {
				$fetch = -1;
			}
		} else {
			$fetch = false;
		}

		$this->closeCursor();
		return $fetch;
	}

	/**
	 * Establece los criterios de GROUP BY para la consulta.
	 * @param string $group_string un string de los criterios de GROUP BY
	 * @return static
	 * Devuelve el objeto mismo.
	 */
	public function groupBy($group_string)
	{
		try {

			switch ($this->action) {
				case self::SELECT:
					$this->groupByString = " GROUP BY $group_string";
					$this->sql = "SELECT $this->selectFields FROM $this->table" . $this->getJoins() . $this->whereString . $this->groupByString . $this->orderByString;
					$this->prepareStatement = $this->prepare(trim($this->sql));
					return $this;
					break;
			}
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	/**
	 * Establece los criterios de ORDER BY para la consulta.
	 * @param string $order_string un string de los criterios de ORDER BY
	 * @return static
	 * Devuelve el objeto mismo.
	 */
	public function orderBy(string $order_string)
	{
		try {

			switch ($this->action) {
				case self::SELECT:
					$this->orderByString = " ORDER BY $order_string";
					$this->sql = "SELECT $this->selectFields FROM $this->table" . $this->getJoins() . $this->whereString . $this->groupByString . $this->orderByString;
					$this->prepareStatement = $this->prepare(trim($this->sql));
					return $this;
					break;
			}
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	/**
	 * Último registro insertado.
	 * @param string $name columna de referencia por defecto NULL
	 * @return mixed
	 * Devuelve el valor de la columna especificada en $name del último registro insertado.
	 */
	public function lastInsertId(string $name = null)
	{
		return $this->db->lastInsertId($name);
	}

	/**
	 * Cantidad de registros de una tabla según una columna.
	 * Funciona igual que 'SELECT columna FROM TABLA' por lo que se debe usa sobre una columna
	 * de la cual se esté seguro que posea un valor en todos los registros
	 * para obtener la cantidad real de registros.
	 * @param string $column Columna que se usará de referencia para el conteo
	 * @return int|boolean
	 * Devuelve el número de registros de una tabla o false en caso de fallar.
	 */
	public function rowCount(string $column = 'id')
	{
		try {
			$sql = "SELECT id FROM $this->table";
			$prepare_statement = $this->db->query($sql);
			$status = $prepare_statement->execute();
			if ($status) {
				$fetch = $prepare_statement->fetchAll();
				if (is_array($fetch)) {
					return count($fetch);
				} else {
					return false;
				}
			} else {
				return false;
			}
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	/**
	 * configDb
	 * 
	 * Establece opciones de la configuración de la base de datos del objeto.
	 * 
	 * @param array $options Las opciones de configuración (ver la constante de clase DEFAULT_OPTIONS_COMMENTS)
	 *
	 * @return void
	 * @throws RequiredOptionMissedException|OptionPatternMismatchException
	 */
	public function configDb(array $options)
	{
		$options = static::_validateOptions($options);
		$driver = $options['driver'];
		$database = $options['database'];
		$host = $options['host'];
		$user = $options['user'];
		$password = $options['password'];
		$charset = $options['charset'];
		$table = $options['table'];

		if ($driver == 'sqlite') {
			$database = strlen($database) == 0 ? ':memory:' : $database;
			$this->db = new PDO("$driver:$database", null, null);
		} else {
			$this->db = new PDO($driver . ":dbname=" . $database . ";host=" . $host, $user, $password);
		}

		$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		if ($driver != 'sqlite') {
			$prepareStatement = $this->db->prepare("SET character set :CHARSET");
			$prepareStatement->execute([':CHARSET' => $charset]);
		}

		if ($this->table === null) {
			$this->setTable($table);
		}else{
			if (is_string($this->prefix_table)) {
				$this->table = trim($this->prefix_table) . trim($this->table);
			}
		}
	}

	/**
	 * Establece la propiedad db.
	 * @param \PDO $database Instancia de \PDO
	 * @return void
	 */
	public function setDb(\PDO $database)
	{
		$this->db = $database;
	}

	/**
	 * Asigna el prefijo tabla del modelo.
	 * @param string $prefix_table Prefijo de la tabla
	 * @return void
	 */
	public function setPrefixTable(string $prefix_table)
	{
		$last_prefix = $this->prefix_table;
		$this->prefix_table = trim($prefix_table);

		if (is_string($this->table)) {

			if (is_string($this->prefix_table)) {

				$this->table = str_replace($last_prefix, '', $this->table);

			}

			$this->setTable($this->table);

		}
	}

	/**
	 * Asigna el nombre del tabla del modelo.
	 * @param string $table Nombre de la tabla
	 * @return void
	 */
	public function setTable(string $table)
	{
		if (is_string($this->prefix_table)) {
			$this->table = trim($this->prefix_table) . trim($table);
		} else {
			$this->table = trim($table);
		}
	}

	/**
	 * Asigna las columnas del modelo.
	 * @param array $fields Array que representa las columnas con la estructura ['columna1','columna2'[,...]]
	 * @return void
	 */
	public function setFields(array $fields)
	{
		$this->fields = $fields;
	}

	/**
	 * setTypeResult
	 * 
	 * Define si el resultados de las consultas de selección de tipo objeto o array asociativo
	 *
	 * @param mixed $assoc Si es true los resultados de las consultas serán un array asociativo
	 * @return void
	 */
	public function setTypeResult(bool $assoc = true)
	{
		$this->type_result_assoc = $assoc;
	}

	/**
	 * setSelectClass
	 *
	 * @param string $nameClass
	 * @return void
	 */
	public function setSelectClass(string $nameClass)
	{
		$this->selectClass = $nameClass;
	}

	/**
	 * configFields
	 * 
	 * Obtiene y configura los nombres de las columnas
	 *
	 * @return boid
	 */
	public function configFields()
	{
		$result = $this->db
			->query("DESCRIBE $this->table")
			->fetchAll(PDO::FETCH_OBJ);

		$this->fields = [];

		foreach ($result as $field_data) {
			$field = $field_data->Field;
			$this->fields[] = $field;
		}

		return $result;
	}

	/**
	 * Devuelve el valor de la propiedad db.
	 * @return \PDO
	 */
	public function getDb()
	{
		return $this->db;
	}

	/**
	 * getPrefixTable
	 * 
	 * Devuelve el prefijo de la tabla
	 *
	 * @return string
	 */
	public function getPrefixTable()
	{
		return $this->prefix_table;
	}

	/**
	 * getTable
	 * 
	 * Devuelve el nombre de la tabla
	 *
	 * @return string
	 */
	public function getTable()
	{
		return $this->table;
	}

	/**
	 * getTableInformation
	 * 
	 * Devuelve la información de la tabla
	 *
	 * @return string
	 */
	public function getTableInformation()
	{
		$result = $this->db
			->query("DESCRIBE $this->table")
			->fetchAll(PDO::FETCH_OBJ);

		return $result;
	}

	/**
	 * getFields
	 *
	 * Devuelve los campos de la tabla
	 * 
	 * @return array
	 */
	public function getFields()
	{
		if ($this->fields === null) {
			$this->configFields();
		}
		return $this->fields;
	}

	/**
	 * Devuelve el string de la consulta
	 * 
	 * @return string
	 */
	public function getCompiledSQL()
	{
		$query = '';

		switch ($this->action) {
			case self::INSERT:
				$query = $this->structureQuery('INSERT', false);
				break;
			case self::SELECT:
				$query = $this->structureQuery('SELECT', false);
				break;
			case self::UPDATE:
				$query = $this->structureQuery('UPDATE', false);
				break;
			case self::DELETE:
				$query = $this->structureQuery('DELETE', false);
				break;
		}

		$replace_values = $this->replacePrepareValues;
		$replace_values_where = $this->whereReplacePrepareValues;


		foreach ($replace_values_where as $alias => $value) {
			$_alias = str_replace(':', "", $alias);
			$query = str_replace($alias, ":($_alias=$value)", $query);
		}

		foreach ($replace_values as $alias => $value) {
			$_alias = str_replace(':', "", $alias);
			$query = str_replace($alias, ":($_alias=$value)", $query);
		}

		return str_replace('  ', " ", $query);
	}

	/**
	 * Devuelve la consulta preparada
	 * 
	 * @return \PDOStatement
	 */
	public function getPrepareStatement()
	{
		try {

			return $this->prepare(trim($this->sql));

		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	/**
	 * debugDumpParams
	 * 
	 * Vuelca un comando preparado de SQL
	 *
	 * @return void
	 */
	public function debugDumpParams()
	{
		$this->prepareStatement->debugDumpParams();
	}

	/**
	 * getReplaceValues
	 *
	 * @return arra
	 */
	public function getReplaceValues()
	{
		return array_merge($this->replacePrepareValues, $this->whereReplacePrepareValues);
	}

	/**
	 * Reinicia todos los JOINS
	 * @return void
	 */
	public function resetJoins()
	{
		$this->joinsString = [];
	}

	/**
	 * Reinicia todos los WHERE
	 * @return void
	 */
	public function resetWhere()
	{
		$this->whereString = '';
		$this->whereReplacePrepareValues = [];
	}

	/**
	 * Reinicia todas los valores internos
	 * @return void
	 */
	public function resetAll()
	{
		$this->type_result_assoc = false;

		$this->prepareStatement = null;

		$this->sql = "";

		$this->selectFields = "*";

		$this->selectString = "";

		$this->insertString = "";

		$this->updateString = "";

		$this->joinsString = [];

		$this->whereString = "";

		$this->orderByString = "";

		$this->action = null;

		$this->groupByString = "";

		$this->replacePrepareValues = [];

		$this->whereReplacePrepareValues = [];
	}

	/**
	 * getLastSQLExecuted
	 *
	 * @return string
	 */
	public function getLastSQLExecuted()
	{
		return $this->lastSQLExecuted;
	}

	/**
	 * Ejecuta la consulta que esté preparada.
	 * @param bool $assoc Define si el resultado es un array asociativo o un objeto
	 * @return boolean|null
	 * Devuelve null si no se ha ha llamado ninguno de los métodos select|insert|update|delete.
	 * Devuelve true en caso de exito o false en caso de error.
	 */
	public function execute(bool $assoc = false)
	{
		try {
			$result = null;
			switch ($this->action) {
				case self::INSERT:
					$result = $this->_executeInsert();
					break;
				case self::SELECT:
					if ($this->type_result_assoc === true || $assoc === true) {
						$result = $this->_executeSelect(true);
					} else {
						$result = $this->_executeSelect(false);
					}
					break;
				case self::UPDATE:
					$result = $this->_executeUpdate();
					break;
				case self::DELETE:
					$result = $this->_executeDelete();
					break;
			}
			$this->prepareStatement = null;
			$this->lastSQLExecuted = $this->sql;
			$this->sql = '';
			$this->resetAll();

			return $result;
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	/**
	 * result
	 * 
	 * Obtiene el resultado de la consulta select
	 *
	 * @return mixed
	 */
	public function result()
	{
		return $this->resultSet;
	}

	/**
	 * Obtiene el string de JOIN
	 * @access private
	 * @return string
	 */
	private function getJoins()
	{
		$str = '';
		$joins = $this->joinsString;
		if (count($joins) > 0) {
			foreach ($joins as $values) {
				$str .= $values;
			}
		}
		return $str;
	}

	/**
	 * Ejecuta la selección.
	 * @param bool $assoc Define si el resultado es un array asociativo o un objeto
	 * @access private
	 * @return array|bool
	 * @throws \PDOException
	 */
	private function _executeSelect(bool $assoc = false)
	{
		$this->structureQuery('SELECT');

		$replace_values = array_merge($this->whereReplacePrepareValues);

		if (count($replace_values) > 0) {
			$status = $this->prepareStatement->execute($replace_values);
		} else {
			$status = $this->prepareStatement->execute();
		}

		if ($status) {

			if ($this->type_result_assoc === true || $assoc === true) {
				$fetch = $this->prepareStatement->fetchAll(PDO::FETCH_ASSOC);
			} else {
				if ($this->selectClass !== null) {
					$fetch = $this->prepareStatement->fetchAll(PDO::FETCH_CLASS, $this->selectClass);
				} else {
					$fetch = $this->prepareStatement->fetchAll(PDO::FETCH_OBJ);
				}
			}

			$this->resultSet = $fetch;

			$this->closeCursor();

		} else {
			$this->resultSet = null;
		}

		return $status;
	}

	/**
	 * Ejecuta la inserción.
	 * @access private
	 * @return bool
	 * @throws \PDOException
	 */
	private function _executeInsert()
	{
		$this->structureQuery('INSERT');
		$execute = $this->prepareStatement->execute($this->replacePrepareValues);
		$this->closeCursor();
		return $execute;
	}

	/**
	 * Ejecuta la actualización.
	 * @access private
	 * @return bool
	 * @throws \PDOException
	 */
	private function _executeUpdate()
	{
		$this->structureQuery('UPDATE');

		$replace_values = array_merge($this->replacePrepareValues, $this->whereReplacePrepareValues);

		$execute = $this->prepareStatement->execute($replace_values);

		$this->closeCursor();

		return $execute;
	}

	/**
	 * Ejecuta la eliminación.
	 * @access private
	 * @return void
	 */
	private function _executeDelete()
	{
		try {

			$this->structureQuery('DELETE');

			if (count($this->whereReplacePrepareValues) > 0) {
				$execute = $this->prepareStatement->execute($this->whereReplacePrepareValues);
			} else {
				$execute = $this->prepareStatement->execute();
			}

			$this->closeCursor();
			return $execute;
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	/**
	 * structureQuery
	 *
	 * @param string $type
	 * @param bool $prepare
	 * @return string
	 */
	private function structureQuery(string $type, bool $prepare = true)
	{
		$type = strtoupper($type);
		switch ($type) {
			case 'SELECT':
				$this->sql = $this->selectString . $this->getJoins() . $this->whereString . $this->groupByString . $this->orderByString;
				break;
			case 'INSERT':
				$this->sql = $this->insertString . $this->getJoins() . $this->whereString;
				break;
			case 'UPDATE':
				$this->sql = $this->updateString . $this->getJoins() . $this->whereString;
				break;
			case 'DELETE':
				$this->sql = $this->deleteString . $this->getJoins() . $this->whereString;
				break;
		}

		$this->sql = trim($this->sql);

		if ($prepare) {
			$this->prepareStatement = $this->prepare(trim($this->sql));
		}

		return $this->sql;
	}

	/**
	 * Cierra el cursor
	 * @access private
	 * @return void
	 */
	private function closeCursor()
	{
		$this->prepareStatement->closeCursor();
	}

	/**
	 * throwException
	 * 
	 * Devuelve una excepción según el tipo escogido
	 *
	 * @param string $type Tipo da la excepción
	 * @return \Exception
	 */
	private function throwException(string $type)
	{
		if ($type = 'malformed_param') {
			return new \Exception('El parámetro recibido no tiene la estructura esperada.');
		}
	}

	/**
	 * setOptionsDb
	 * 
	 * Establece opciones de la configuración de la base de datos 
	 * Nota: las opciones de la clase, no del objeto que puede ser diferente
	 * si se definen en el constructor.
	 * 
	 * @param array $options Las opciones de configuración (ver la constante de clase DEFAULT_OPTIONS_COMMENTS)
	 *
	 * @return void
	 * @throws RequiredOptionMissedException|OptionPatternMismatchException
	 */
	public static function setOptionsDb(array $options)
	{
		static::$optionsDb = static::_validateOptions($options);
	}

	/**
	 * getOptionsDb
	 * 
	 * Devuelve las opciones de la configuración de la base de datos 
	 * Nota: las opciones de la clase, no del objeto que puede ser diferente
	 * si se definen en el constructor.
	 *
	 * @return array
	 */
	public static function getOptionsDb()
	{
		if(count(static::$optionsDb)>0){
			return static::$optionsDb;
		}else{
			return static::$defaultValueOptions;
		}
	}

	/**
	 * validateOptions
	 *
	 * @param array $options Las opciones de configuración (ver la constante de clase DEFAULT_OPTIONS_COMMENTS)
	 * @return bool
	 */
	public static function validateOptions(array $options)
	{
		try {
			static::_validateOptions($options);
			return true;
		} catch (RequiredOptionMissedException | OptionPatternMismatchException $e) {
			return false;
		}
	}

	/**
	 * _validateOptions
	 *
	 * @param array $options Las opciones de configuración (ver la constante de clase DEFAULT_OPTIONS_COMMENTS)
	 * @return array Las opciones
	 * @throws RequiredOptionMissedException|OptionPatternMismatchException
	 */
	private static function _validateOptions(array $options)
	{

		$default_options = static::$defaultValueOptions;

		$patterns_options = static::$patternsOptions;

		foreach ($patterns_options as $option => $pattern) {
			if (!array_key_exists($option, $options) || is_null($options[$option])) {
				if (array_key_exists($option, $default_options)) {
					$options[$option] = $default_options[$option];
				}
			}
		}
		$traverser = new Traverser(new ValidatorLocator());


		foreach ($patterns_options as $option => $pattern) {
			if (!array_key_exists($option, $options)) {
				throw new RequiredOptionMissedException($option);
			}
			$option_validate = [$option => $options[$option]];
			if (!$traverser->check($pattern, $option_validate)) {
				throw new OptionPatternMismatchException($option, $pattern[$option]);
			}
		}

		return $options;
	}

	/**
	 * Conversión a string
	 * @return string
	 */
	public function __toString()
	{
		$data = [];

		$data['db'] = $this->db;

		$data['table'] = $this->table;

		$data['prefix_table'] = $this->prefix_table;

		$data['fields'] = $this->fields;

		$data['type_result_assoc'] = $this->type_result_assoc;

		$data['prepareStatement'] = $this->prepareStatement;

		$data['sql'] = trim($this->sql);
		$data['compiledSQL'] = $this->getCompiledSQL();
		$data['lastSQLExecuted'] = trim($this->lastSQLExecuted);

		$data['selectFields'] = $this->selectFields;

		$data['selectString'] = $this->selectString;
		$data['insertString'] = $this->insertString;
		$data['updateString'] = $this->updateString;
		$data['deleteString'] = $this->deleteString;
		$data['joinsString'] = $this->joinsString;
		$data['whereString'] = $this->whereString;
		$data['orderByString'] = $this->orderByString;

		$data['action'] = $this->action;

		$data['groupByString'] = $this->groupByString;

		$data['replacesValues'] = $this->getReplaceValues();

		$data['resultSet'] = $this->resultSet;

		return json_encode($data);
	}



	/**
	 * Conversión a string
	 * @return string
	 */
	public function jsonSerialize()
	{
		$data = [];

		$data['db'] = $this->db;

		$data['table'] = $this->table;

		$data['prefix_table'] = $this->prefix_table;

		$data['fields'] = $this->fields;

		$data['type_result_assoc'] = $this->type_result_assoc;

		$data['prepareStatement'] = $this->prepareStatement;

		$data['sql'] = trim($this->sql);
		$data['compiledSQL'] = $this->getCompiledSQL();
		$data['lastSQLExecuted'] = trim($this->lastSQLExecuted);

		$data['selectFields'] = $this->selectFields;

		$data['selectString'] = $this->selectString;
		$data['insertString'] = $this->insertString;
		$data['updateString'] = $this->updateString;
		$data['deleteString'] = $this->deleteString;
		$data['joinsString'] = $this->joinsString;
		$data['whereString'] = $this->whereString;
		$data['orderByString'] = $this->orderByString;

		$data['action'] = $this->action;

		$data['groupByString'] = $this->groupByString;

		$data['replacesValues'] = $this->getReplaceValues();

		$data['resultSet'] = $this->resultSet;


		return $data;
	}

}
