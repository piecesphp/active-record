<?php

/**
 * RequiredOptionMissedException.php
 */

namespace PiecesPHP\Core\Database\Exceptions;

/**
 * RequiredOptionMissedException - Excepción para cuando falta una opción obligatoria
 *
 * @package     PiecesPHP\Core\Database\Exceptions
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @version     v.1.0.0
 * @copyright   Copyright (c) 2018
 */
class RequiredOptionMissedException extends \Exception
{
	/**
	 * __construct
	 *
	 * @param string $optionName
	 * @return static
	 */
	public function __construct(string $optionName)
	{
		parent::__construct("La opción $optionName es requerida.");
	}

}
