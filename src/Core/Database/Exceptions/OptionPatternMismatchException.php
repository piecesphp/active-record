<?php

/**
 * OptionPatternMismatchException.php
 */

namespace PiecesPHP\Core\Database\Exceptions;

/**
 * OptionPatternMismatchException - Excepción para opción no coincide con el patrón
 *
 * @package     PiecesPHP\Core\Database\Exceptions
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @version     v.1.0.0
 * @copyright   Copyright (c) 2018
 */
class OptionPatternMismatchException extends \Exception
{
	/**
	 * __construct
	 *
	 * @param string $optionName
	 * @param string $patternName
	 * @return static
	 */
	public function __construct(string $optionName, strins $patternName)
	{
		parent::__construct("Se esparaba que la opción $optionName coincidiera con el patrón: $patternName");
	}

}
